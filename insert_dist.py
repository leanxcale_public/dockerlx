from sqlalchemy import *
from sqlalchemy.orm import *
from threading import Thread


def worker(kmin, kmax):
    print('Worker:', kmin)
    param = []
    for i in range(kmin, kmax):
        param.append({'f1': i, 'f2': 'string1', 'f3': 0})
    db = create_engine('leanxcale://APP@172.28.1.7:1529/tpch')
    Session = sessionmaker(autocommit=False)
    Session.configure(bind=db)
    sess = Session()
    meta = MetaData(bind=db)
    meta.reflect(db)
    table = Table('TABLE_8', meta,
                       Column('f1', Integer, primary_key=True),
                       Column('f2', String ),
                       Column('f3', Integer), extend_existing=True)

    try:
         sess.execute(table.insert(),param)
         sess.commit()
         print("Commited")
    except Exception as e:
        print(e)
        sess.rollback()
        print("Rollback")
    sess.close()


if __name__ == "__main__":
    p1 = Thread(target=worker, args=(0, 500,))
    p2 = Thread(target=worker, args=(501, 1001,))
    
    p1.start()
    p2.start()
 


