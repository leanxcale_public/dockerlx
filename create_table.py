import sqlalchemy
from sqlalchemy.ext.declarative import declarative_base
import random

if __name__ == "__main__":
    db = sqlalchemy.create_engine('leanxcale://APP@172.28.1.7:1529/tpch')

    conn = db.connect().execution_options(autocommit=True)
    base = declarative_base()
    conn.execute("CREATE TABLE TABLE_8 (f1 int, f2 VARCHAR, f3 int, PRIMARY KEY (f1))")
    conn.close()
